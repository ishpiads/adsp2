'use strict';

/* concat two arrays */
var concat = function(a, b) { return a.concat(b); };

/* uppercase string */
var upper = function(s) { return s.toUpperCase(); };

/* create drug structure */
var createDrugStruct = function(drug, i) {
	var brand = drug.openfda.brand_name[0];		// brand
	var ndc = drug.openfda.package_ndc[i];		// national drug code (package)

	// return the drug struct
	return {
		brand: brand,
		ndc: ndc,
		name: brand + " (" + drug.openfda.manufacturer_name[0] + ", " + ndc + ")",
		drug: drug
	};
};

/* smarter sort */
var sort = function(arr) {
    // temporary array holds objects with position and sort-value (i.e., name)
    var values = arr.map(function(e, i){ return { index: i, value: e }; });

    // sorting the mapped array containing the reduced values
    values.sort(function(a, b){ return +(a.value > b.value) || +(a.value === b.value) - 1; });
    
    // container for the resulting order
    return values.map(function(e){ return arr[e.index]; });
};

/* controllers */

angular.module('controllers', ['ui.bootstrap'])

// drug search controller
.controller('DrugsCtrl', ['$scope', '$http', 'Drugs',
  function($scope, $http, Drugs) {
	
	// statuses
	$scope.dosage_status = 0;
	$scope.interactions_status = 0;
	$scope.reactions_status = 0;
	
	// scoped function, takes the drug item selected and scopes it to the page for display
    $scope.onSelect = function(item) {
      $scope.drug = item.drug;
      $scope.name = item.brand;
      $scope.ndc = item.ndc;
      $scope.manufacturer = item.drug.openfda.manufacturer_name[0];
      
      // set statuses
      $scope.dosage_status = item.drug.dosage_and_administration ? item.drug.dosage_and_administration.length : 0;
      $scope.interactions_status = item.drug.drug_interactions ? item.drug.drug_interactions.length : 0;
      
      // get the adverse effects
      effects($scope.ndc);
    };

    /* scoped function, takes the value typed in and queries openfda for substance or brand name */
    $scope.getDrugs = function(val) {
      // build query for drugs by brand and only where brand exists
      var query = 'brand_name:'.concat(val).concat('+AND+_exists_:brand_name');
      
      return $http.get('https://api.fda.gov/drug/label.json', {
        params: {
          search: query,
          limit: 100,
          api_key: "JOykJT496zwUqfLmnTaeJBj3KmtpuuR0UhYJm1VD"
        }
      }).then(function(response){
        /* predicate to determine whether value matches and is unique */
        var unique_match = function(v,i,a) {
          // check that the brand starts w/ the query value & item is not a duplicate
          return v.brand.toUpperCase().indexOf(val.toUpperCase()) > -1 && a.indexOf(v) == i;
        };

        var rs = response.data.results.map(function(drug) {
          return drug.openfda.package_ndc.map(function(ndc, i){
        	  return createDrugStruct(drug, i);
          });
	      // create an array of items based on  and drug
	      //return createDrugStruct(drug);
        });

        // results
        rs = rs
          // flatten results
          .reduce(concat)
          // filter matches and uniques
          .filter(unique_match);

        // temporary array holds objects with position and sort-value (i.e., name)
        var names = rs.map(function(e, i) { return { index: i, value: e.name }; });

        // sorting the mapped array containing the reduced values
        names.sort(function(a, b) { return +(a.value > b.value) || +(a.value === b.value) - 1; });
        
        // container for the resulting order
        return names.map(function(e){ return rs[e.index]; });
      }, function(err) {
    	  // error encountered, no results
    	  return [];
      });
    };
    
    /* once an item is selected, use its package ndc to get related effects */
    var effects = function(ndc) {
      $scope.reactions_status = -1;
    	
      // build query for drugs by brand and only where brand exists
      var query = 'package_ndc:"'.concat(ndc).concat('"');
      $http.get('https://api.fda.gov/drug/event.json', {
        params: {
          search: query,
          limit: 100,
          api_key: "JOykJT496zwUqfLmnTaeJBj3KmtpuuR0UhYJm1VD"
        }
      }).then(function(response){
        /* predicate to determine whether value matches and is unique */
        var unique_match = function(v,i,a) {
          // check that the brand starts w/ the query value & item is not a duplicate
          return a.indexOf(v) == i;
        };
        
        /* retrieve medicines
        $scope.medicines = sort(response.data.results.map(function(event){
        	return event.patient.drug.map(function(drug){
        		return drug.medicinalproduct;
        	});
        })
        .reduce(concat)
        .filter(unique_match));
        */
    	
        // retrieve reactions
        $scope.reactions = sort(response.data.results.map(function(event){
        	return event.patient.reaction.map(function(reaction){
        		return event.companynumb + ": " + reaction.reactionmeddrapt;
        	});
        })
        .reduce(concat)
        .filter(unique_match));
 
        // set the status
        $scope.reactions_status = $scope.reactions.length;
      }, function(error) {
      	// no results
      	$scope.medicines = null;
      	$scope.reactions = null;
      	$scope.reactions_status = 0;
      });
    };
  }]
);