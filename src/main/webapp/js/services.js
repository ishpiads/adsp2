'use strict';

/* Services */

angular.module('services', ['ngResource'])
  .factory('Drugs', ['$resource',
    function($resource){
	  return $resource('https://api.fda.gov/drug/label.json',
	    { search : "" },
	    { search : {method:'GET', isArray:false}});
    }]);