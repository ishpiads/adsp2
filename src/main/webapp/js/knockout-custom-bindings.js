ko.bindingHandlers.sort = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var asc = false;
        element.style.cursor = 'pointer';
        $(element).addClass("header");
        
        element.onclick = function(){
            var value = valueAccessor();
            var prop = value.prop;
            var data = value.arr;
            
            asc = !asc;
            if(asc){
                $(this).addClass("headerSortUp");
                $(this).removeClass("headerSortDown");
                data.sort(function(left, right){
                    return left[prop]() == right[prop]() ? 0 : left[prop]() < right[prop]() ? -1 : 1;
                });
            } else {
                $(this).removeClass("headerSortUp");
                $(this).addClass("headerSortDown");
                data.sort(function(left, right){
                    return left[prop]() == right[prop]() ? 0 : left[prop]() > right[prop]() ? -1 : 1;
                });
            }
        }
    }
};