var index_namespace = index_namespace || {};

index_namespace._construct = function() {
var ns = this;
var newUser = {id: '', firstName: '', lastName: '', email: ''};
var viewModel;

this.PeopleModel = function(data, parent){
    var self = this;
    var parent = parent;
    ko.mapping.fromJS(data, {}, self); //from JSON data   
    self.isChecked = ko.observable(false);
    
    //Determines whether or not we are editing a user or adding one.
    self.isAdd = ko.observable(false);
    
    self.saveUser = function(user){        
        //Clone the object otherwise when we try and create more than two we get in trouble.
        if (user.isAdd()){ 
        	userJSON = ko.toJS(user);
        	
        	//{"firstName": "david", "lastName": "last", "email": "supcom234@gmail.com"}
        	ajaxPost("rs/person/add", userJSON, function(data){        		
        		parent.people.push(new ns.PeopleModel(data, parent));
        	});
             
            ko.mapping.fromJS(newUser, {}, user);
        } else {
        	ajaxPut("rs/person/update/" + user.id(), ko.toJS(user), function(data){
        		self.id(user.id());
                self.firstName(user.firstName());
                self.lastName(user.lastName());
                self.email(user.email());	
        	});            
        }
        
        $('#addUserModal').modal('hide')        
        //Everytime we save we have to trigger an update on the table otherwise the tablesorter does not work as intended
        $("#userTable").trigger("update");                
    }
    
    self.editUser = function(user){
        var clonedUser = ko.mapping.fromJS(ko.toJS(user));
        clonedUser.isAdd(false);
        parent.addPerson(clonedUser);
        //parent.addPerson.isAdd(false);
        $('#addUserModal').modal({backdrop: 'static', keyboard: false}).modal('show');
    }
}

this.ViewModel = function(data){
    var self = this;
    ko.mapping.fromJS(data,{
		'people':{
            key: function(data) {                
                return ko.utils.unwrapObservable(data.id)
            },
			create: function(options){                
				return new ns.PeopleModel(options.data, self);
			}
		}		
	}, self);
    
    //self.addPerson = ko.observable(new PeopleModel(newUser, self));    
    self.addPerson = ko.observable();    
    self.isCheckedUsers = ko.observable(false);
    
    self.selectAllUsers = function() {        
        var ary = self.people();        
        var val = self.isCheckedUsers();        
        for (i in ary){
            ary[i].isChecked(val);
        }    
        return true; //must return true otherwise we bug out
    }
    
    self.removeUsers = function() {
        for (i in self.people()){
            if (self.people()[i].isChecked()){
                $('#alertModal').modal({backdrop: 'static', keyboard: false}).modal('show');    
                return true;
            }
        }
        return false;
    }
    
    self.removeUsersConfirm = function() {        
        self.people.remove(function(user) {
        	if (user.isChecked()){
        		//TODO Consider refactoring later because if this generates a delete request for every checked user which can be alot.
        		ajaxDelete("rs/person/delete/" + user.id(), function(data, textStatus, jqXHR){            		
            		if (jqXHR.status != 204){
            			console.log("Something bad happened here!!!");
            		}
            	});
        	}        	
        	return user.isChecked(); 
        });
        $('#alertModal').modal('hide');
        $("#userTable").trigger("update"); 
    }
    
    self.addUser = function(){
        self.addPerson(new ns.PeopleModel(newUser, self));       
        self.addPerson().isAdd(true);
        $('#addUserModal').modal({backdrop: 'static', keyboard: false}).modal('show');
    }
}

this.PlotGraph = function(){
	var url = "https://query.yahooapis.com/v1/public/yql?q=select%20Close%2C%20Date%20from%20yahoo.finance.historicaldata%20where%20symbol%20%3D%20%22YHOO%22%20and%20startDate%20%3D%20%222009-09-11%22%20and%20endDate%20%3D%20%222010-03-10%22&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=";
	var points = [];
	var opts = {
			xaxis: { mode: "time" },
            series: {
				lines: {
					show: true
				},
				points: {
					show: true
				}
			},
            colors: ["#138900"],
            grid: {
				hoverable: true,
				clickable: true
			}
		};
	
    ajaxGet(url, function(data){
        var yahooQuote = data.query.results.quote;        
        for (i in yahooQuote){            
            var dataSet = yahooQuote[i];
            var point = [Date.parse(dataSet.Date), parseFloat(dataSet.Close)];
            points.push(point);            
        }
         $.plot("#placeholder", [ { data: points, label: "Close Price"} ], opts);
        
        $("<div id='tooltip'></div>").css({
			position: "absolute",
			display: "none",
			border: "1px solid #fdd",
			padding: "2px",
			"background-color": "#fee",
			opacity: 0.80
		}).appendTo("body");
        
        $("#placeholder").bind("plothover", function (event, pos, item) {						
            if (item) {
                var x = new Date(item.datapoint[0]).toDateString();
                var y = item.datapoint[1].toFixed(2);

                $("#tooltip").html(item.series.label + " on " + x + " = " + y)
                    .css({top: item.pageY+5, left: item.pageX+5})
                    .fadeIn(200);
            } else {
                $("#tooltip").hide();
            }
			
		});
        
        // Add the Flot version string to the footer
        $("#footer").prepend("Flot " + $.plot.version + " &ndash; ");
    });
    
    $(window).resize(function() {$.plot($('#placeholder'), [ { data: points, label: "Close Price"} ], opts);});
}
} //end namespace
index_namespace._construct();