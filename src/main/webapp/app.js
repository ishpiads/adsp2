'use strict';

/* app */

angular.module('ads', [
  'ngRoute',
  'controllers',
  'services'
])

/* filter for trucating long text */
.filter('cut', function () {
    return function (value, wordwise, max, tail) {
        if (!value) return '';

        value = value.toUpperCase();
        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
        if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace != -1) {
                value = value.substr(0, lastspace);
            }
        }

        return value + (tail || ' �');
    };
})

.filter('pluralize', function(){
	return function(count) {
		//switch(count) {
		//	case 0: return "";
		//	case 1: return "1 results";
		//	default: return n + " results";
		//}
		switch (count) {
			case -1: return "loading...";
			case 0: return "";
			case 1: return "1 item";
			default: return count + " items";
		};
	};
})
    
.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/phones', {
        templateUrl: 'partials/phone-list.html',
        controller: 'PhoneListCtrl'
      })
      .when('/phones/:phoneId', {
        templateUrl: 'partials/phone-detail.html',
        controller: 'PhoneDetailCtrl'
      })
      .otherwise({
        redirectTo: '/phones'
      });
  }])
  // modifying httpProvider to permit joining of '+' symbol in uri
  .config(function($httpProvider) {
    $httpProvider.interceptors.push(function($q) {
      var realEncodeURIComponent = window.encodeURIComponent;
      return {
        'request': function(config) {
           window.encodeURIComponent = function(input) {
             return realEncodeURIComponent(input).split("%2B").join("+"); 
           }; 
           return config || $q.when(config);
        },
        'response': function(config) {
           window.encodeURIComponent = realEncodeURIComponent;
           return config || $q.when(config);
        }
      };
    });
  });
