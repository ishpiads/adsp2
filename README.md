Development Prototype: [http://ishpip2prod.appspot.com/](http://ishpip2prod.appspot.com/)

Evaluation Branch: [https://bitbucket.org/ishpiads/adsp2/src](https://bitbucket.org/ishpiads/adsp2/src)

# 750-WORD DESCRIPTION: #

Based on the RFQ and available information, ISHPI senior management analyzed project needs and identified the team coach, the team leader, and the development team (Evidence#02_Exhibits-1,2).  After reviewing the problem domain, the development team identified the technology stack (Evidence#01) and tailored ISHPI�s standard process to accomplish project goals. 

## Outcome: ##
ISHPI�s development prototype consists of an AngularJS-based single page application built with a model-view-controller design. Team used Jasmine framework to perform unit testing of JavaScript-based components. Test specifications were written against application controllers to ensure proper data processing and transformation before rendering to view templates. Team employed the Twitter Bootstrap framework to ensure consistent cross platform user experience.  Team set up automated builds, testing, and deployment through integrating Maven, Jasmine, Jenkins, and Google App Engine.  Throughout, the team followed disciplined agile practices and demonstrated improved engineering capability and estimation accuracy (Actual vs. Plan 1.23 to 0.95) (Evidence#07).

## Progression: ##
* Sprint-1
    * Retrieved and displayed basic drug data
    * Created initial development infrastructure
* Sprint-2
    * Incorporated demo feedback
        * Implemented autocomplete
        * Added drug interaction
    * Set up Jenkins and Google App Engine environment
* Sprint-3
    * Incorporated demo feedback 
        * Usability enhancements
        * Added drug dosage instructions 
    * Set up QUnit environment
* Sprint-4
    * Incorporated demo feedback 
        * Usability enhancements
        * Added drug adverse effects
        * Mobile device compatibility
    * Set up Jasmine test framework and automation
* Sprint-5
    * Implemented customer�s high-priority bug fix to ensure mobile device compatibility
* Sprint-6
    * Deployed Docker image
    * Enhanced continuous monitoring

## Approach: ##
Team went through a team launch facilitated by a coach, during which:

* The Product Owner described customer�s vision, goals, and product features (Evidence#03).  
* The development team elicited clarifications on goals, targeted users, top priorities, criteria for fully successful product, and criteria for minimum viable product (Evidence#03).
* Considering customer priorities, team identified user stories, their associated tasks, and other work items (Evidence#04,#05).
* Team identified the following development strategy (Evidence#01_Exhibits-7,8):
    * Create necessary technical infrastructure for continuous integration and automated testing at the earliest
    * Implement slices of high-priority user stories iteratively through daily sprints
    * Conduct daily user demos and incorporate feedback into the next sprint
    * Implement basic user interface (since in a real project, UI design would have been done by a design team)
* Team agreed on the following development process (Evidence#01_Exhibit-9):
    * Author analyzes, designs, codes, and unit tests the component
    * Once implemented, a peer review is conducted
    * Once reviewed, functional evaluation is performed
* Team discussed and agreed on the following configuration management practices (Evidence#01_Exhibits-4,5):
    * Create a �master� branch, from which a �test� branch is created immediately, and from which a �sprint� branch is created. 
    * Authors create �ticket specific� branch off of the �sprint� branch.
    * Once developed, author merges with the sprint branch, which is verified during peer review.
    * Once all targeted tickets are through peer review, the sprint branch is merged with the test branch.
    * Once testing is complete, test branch is merged with the mater branch.
* Team decided to:
    * collocate
    * conduct one Scrum Meeting per day, daily user demo, and daily retrospective
    * track actual effort data

## Sprint Planning (Evidence#05): ##
* Team used relative effort method (very small, small, medium, large, and very large) to estimate the work quickly (Evidence#01_Exhibit-06).
* Team used ISHPI�s historical data for Sprint-1 and team�s actual data for Sprint-2 to Sprint-6 (Evidence#06).
* Based on customer priorities and the development strategy, the team identified sprint's scope.
* Team discussed and documented the definition of done.
* After team�s consent, sprint goals and the definition of done were reviewed with the customer.
* After customer�s approval, sprint was started.

## Sprint Execution (Evidence#08): ##
* Team members picked their initial tasks that are aligned with the sprint goal and development strategy.
* Team collaborated with each other and resolved technical issues effectively and efficiently.
* After implementation, peer review was conducted and merging conflicts were resolved.
* Once peer reviewed, functional evaluation was conducted.
* Team conducted user demo and feedback was elicited for future enhancements and priorities (Evidence#09).
* Team tracked actual effort in JIRA tickets (Evidence#07).

## Retrospective (Evidence#10): ##
* Team conducted a retrospective after each sprint.
* Reviewed facts:
    * Sprint goals
    * Customer feedback from demo
    * Estimation accuracy
    * Team�s definition of done
* Contemplated on:
    * what worked well
    * what needs improvement
    * what to stop
* Agreed on the definition of done for the next sprint.